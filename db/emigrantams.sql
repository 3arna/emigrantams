/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : emigrantams

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-07-03 16:04:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ads`
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `id` int(10) NOT NULL DEFAULT '0',
  `category` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `title` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `teaser` varchar(10000) CHARACTER SET utf8 DEFAULT NULL,
  `salary` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `aboutwork` text CHARACTER SET utf8,
  `livingconditions` text CHARACTER SET utf8,
  `workplacedistance` text CHARACTER SET utf8,
  `language` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `exp` text CHARACTER SET utf8,
  `name` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `skype` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `town` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `workplace` text CHARACTER SET utf8,
  PRIMARY KEY (`category`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ads
-- ----------------------------
INSERT INTO `ads` VALUES ('0', 'none', '2011-05-05', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla');
INSERT INTO `ads` VALUES ('3', '\r\n                                    none', '2011-05-30', '\r\n                        Pvz.: Darbuotojas gyvuliu ukije (papildomas darbas, puse metu laikotarpiui)', '\r\n                       Smulkus daro aprasymas', '\r\n                       Pvz.: 105 Lt/men ', 'Pvz.: Šerti, prižiųrėti, tvarkyti fermą.', '\r\n             Pvz.: Atskiras namelis nuo Å¡eimininkÅ³, virtuvÄ—, duÅ¡as, toletas, miegamasis.\n            Virtuve, duÅ¡u, toletu dalintis su dar pora Å¾moniÅ³.', '\r\n            Pvz.: 30min. vaÅ¾iuojant automobiliu.', '\r\n                     Pvz.: Anglu, Lietuviu, Azeraidziano', '\r\n                          Pvz.: Jokios nereikia ne is medzio iskrites.', '\r\n                         Pvz.: Alfredas', '\r\n                  ', '\r\n                        Pvz.: as@as.lt', '\r\n                        Pvz.: alfa123', '\r\n                       Airija, Dublinas, Blue Roar 123', '\r\n                                   country', '\r\n                                      town', '\r\n                     Pvz.: Darbas Airijoje, galviju fermoje.\r\n            ');
INSERT INTO `ads` VALUES ('2', '\r\n                                    none', '2011-05-30', '\r\n                        Pvz.: Darbuotojas gyvuliu ukije (papildomas darbas, puse metu laikotarpiui)', '\r\n                       Smulkus daro aprasymas', '\r\n                       Pvz.: 105 Lt/men ', '\r\n                    Pvz.: Å erti, priÅ¾iÅ³rÄ—ti, tvarkyti fermÄ….', '\r\n             Pvz.: Atskiras namelis nuo Å¡eimininkÅ³, virtuvÄ—, duÅ¡as, toletas, miegamasis.\n            Virtuve, duÅ¡u, toletu dalintis su dar pora Å¾moniÅ³.', '\r\n            Pvz.: 30min. vaÅ¾iuojant automobiliu.', '\r\n                     Pvz.: Anglu, Lietuviu, Azeraidziano', '\r\n                          Pvz.: Jokios nereikia ne is medzio iskrites.', '\r\n                         Pvz.: Alfredas', '\r\n                  ', '\r\n                        Pvz.: as@as.lt', '\r\n                        Pvz.: alfa123', '\r\n                       Airija, Dublinas, Blue Roar 123', '\r\n                                   country', '\r\n                                      town', '\r\n                     Pvz.: Darbas Airijoje, galviju fermoje.\r\n            ');

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phonenr` varchar(100) DEFAULT NULL,
  `access` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `booked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('0', '2011-05-12', 'Jonas@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('1', '2011-05-13', 'Jonas@pvza.lt', 'Jonoslapt11', '', 'admin', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('2', '2011-05-13', 'Jonas2@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('3', '2011-05-13', 'JonasBalciunas@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('4', '2011-05-13', 'safjkl@fsaknm.lt', 'tomas', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('5', '2011-05-13', 'Jon2323as@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('6', '2011-05-14', 'Jonaasdasds@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('7', '2011-05-14', 'Jonas1@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('8', '2011-05-18', 'Jonas3924@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('9', '2011-05-18', 'JonasJonas@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('10', '2011-05-18', 'JonasKazlauskas@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('11', '2011-05-18', 'JonasKazlauskius@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('12', '2011-05-18', 'Jonasslepinskis@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('13', '2011-05-19', 'Jonassaasas@pvz.lt', 'Jonoslapt11', '', 'user', '2011-05-03 16:03:23');
INSERT INTO `members` VALUES ('14', '2011-05-28', 'Vladas@pvz.lt', 'vladasgaidys', '', 'user', '2011-05-03 16:03:23');

-- ----------------------------
-- Table structure for `text`
-- ----------------------------
DROP TABLE IF EXISTS `text`;
CREATE TABLE `text` (
  `id` int(11) NOT NULL DEFAULT '0',
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of text
-- ----------------------------
INSERT INTO `text` VALUES ('0', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('1', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('2', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('3', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('4', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('5', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('6', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('7', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('8', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('9', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('10', 'CBB bus ramybi');
INSERT INTO `text` VALUES ('11', 'CBB bus ramybi');

-- ----------------------------
-- Table structure for `worklist`
-- ----------------------------
DROP TABLE IF EXISTS `worklist`;
CREATE TABLE `worklist` (
  `id` int(10) NOT NULL DEFAULT '0',
  `accid` int(10) DEFAULT NULL,
  `workid` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of worklist
-- ----------------------------
