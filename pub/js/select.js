$(document).ready(function(){ 
    $(".grey_box").click(function(){
        $(".selects").hide();
        var parent = $(this).parent().parent();
        $(parent).children(".selects").show();
        var position = $(this).parent().position();
        var selects = $(parent).children(".selects");
        $(selects).css({"top" : position.top + 25, "left" : position.left});
        var li = $(parent).children(".selects").children("li");
        $(li).click(function(){
            var text = $(this).text();
            $(parent).children(".select_string").children(".white_box").text(text);
            $(parent).children(".selects").hide();
        })
    })
 });