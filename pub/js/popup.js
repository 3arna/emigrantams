$(document).ready(function(){
    
    /* Echo value to popup */
    
    function showinpopup(html){
        $("#popup").html(html)
        popupcenter();
        
    }
    
    
    /* LOGIN */
    load("#header_up_mid_up", "login");
    
    $("#page .submit").live('click', function(){
        
        
        var value = submitform("#page", "login");
        //showinpopup(value);
        if(value == "admin"){
            load("#header_up_mid_up", "admin");
        }
        if(value == "user"){
            load("#header_up_mid_up", "user");
        }
    });
    
    
    /* POPUP */
    
    $(".popup-open").live('click', function(){
        
        var content = "#popup";
        var app = $(this).attr("html");
        
        load(content, app);
        
        $(content+" .submit").live('click', function(){
        
            var value = submitform(content, app);
            showinpopup(value);
            if(value == "true"){
                $(content).load("../app/"+app+"/success/success.view.html", function(){
                    popupcenter();
                });
                
            }
        });
        
    });
    
    function popupcenter(){
        var docheight = $(document).height();
        var docwidth = $(document).width();
        var winheight = $(window).height();
            
        $("#popup_shadow").height(docheight);
        var scrolled_pos = $(window).scrollTop();
            
        $(".popup").show();
        var popupheight = $("#popup").height();
        var popupwidth = $("#popup").width();
        if(popupheight>winheight){
            $("#popup-content").css("top", 30);
        }
        else{
            $("#popup-content").css("top",(winheight/2+scrolled_pos)-popupheight/2);
        }
        $("#popup-content").css("left",(docwidth/2)-popupwidth/2);
    }
    
    function submitform(form, app){
        
        var icount = $(form+" input").size();
        var tcount = $(form+" textarea").size();
        var values = [];
        var names = [];
                
        for(var i=0; i<icount; i++){
            var name = $(form+" input").eq(i).attr("name");
            var value = $(form+" input").eq(i).attr("value");
            values[i]=value;
            names[i]=name;
        }
        
        if(tcount>0){
            for(var i=0; i<tcount; i++){
                var name = $(form+" textarea").eq(i).attr("name");
                var value = $(form+" textarea").eq(i).attr("value");
                values[(i-1+tcount)]=value;
                names[(i-1+tcount)]=name;
            }
        }
        
        var value = $.ajax({
            url: "?u="+app+"&a="+app,
            data: ({
                'values[]': values,
                'names[]': names
            }),
            type: "POST",
            async:false
        }).responseText;
        
        return value;
        
    }
    
    
    function load(content, app){
        
        $(content).html("").load("?u="+app+"&a=view", function() {
            if(content == "#popup"){
                popupcenter();
            }
        })
        
    }
    
    
    $("#popup_shadow, #popup-close").click(function(){
        $(".popup").hide();
    });
    
 });