<?php
    
    class select extends db{
        
        public $table;
        protected $result;
        
        function __construct($table){
            $this->table = $table;
        }
        
        function __SelectAll(){
            $query = "SELECT * FROM {$this->table}";
            $this->Query($query);
            return $this->Output();
        }
        
        function __SelectQuery($query){
            $this->Query($query);
            return $this->Output();
        }
        
        
        
        
        function Output(){
            $numfields = mysql_num_fields($this->result);
            
            $fields = array();
            $result = array();
            
            for($i=0; $i<$numfields; $i++){
                $fields[] = mysql_field_name($this->result, $i);
            }
            
            //array_push($result, $fields);
            
            $y=0;
            while($row = mysql_fetch_array($this->result)){
                
                for($i=0; $i<$numfields; $i++){
                    $result[$fields[$i]][$y]=$row[$fields[$i]];
                }
            $y++;
            }
            
            return $result;
        }
        
    }