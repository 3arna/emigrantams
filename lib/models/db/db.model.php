<?php
    
    class db{
        
        private $server;
        private $username;
        private $password;
        public $database;
        private $cfg;
        
        
        function __construct(){
            $this->LoadCfg();
            mysql_connect($this->server,$this->username,$this->password);
            $this->LoadDb($this->database);
        }
        
        function LoadDb($database){
            if($database){
                $this->database = $database;
            }
            mysql_select_db($this->database);
        }
        
        function Query($query){
            mysql_real_escape_string($query);
            $this->result = mysql_query($query);
            
        }
        
        function __Count(){
            $num = mysql_num_rows($this->result);
            return $num;
        }
        
        function LoadCfg(){
            $this->cfg = ROOT.DS.'cfg'.DS.'db.ini.php';
            $file = parse_ini_file($this->cfg);
            $this->server=$file['server'];
            $this->username=$file['username'];
            $this->password=$file['password'];
            $this->database=$file['db'];
        }
        
        /*function __destruct(){
            mysql_close();
        }
        */
    }