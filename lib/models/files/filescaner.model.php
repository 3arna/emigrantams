<?php
    

     class file_filter{

        var $cleandir = array();
        var $files = array();
        public $url = '';
        public $cfg;
        public $cur_url;
        public $pre = '';

        function __construct($url,$pre,$cfg){
            
            $this->url = $url;
            $this->pre = $pre;
            if($cfg){
                $this->cfg = ROOT.DS.'cfg'.DS.$cfg;
            }
            $this->dir_scan();
            $this->rewrite_cfg();

            
            
        }

        function dir_scan(){
            $dir = scandir($this->url.DS.$this->cur_url);
            $cleandir = array();
            for($i=0; $i<count($dir); $i++){
                if($dir[$i]!='.' && $dir[$i]!='..'){
                    if(strpos($dir[$i], $this->pre)){
                        array_push($this->files,$this->cur_url.DS.$dir[$i]);
                    }
                    elseif(scandir($this->url.DS.$this->cur_url.DS.$dir[$i])){
                        array_push($this->cleandir,$this->cur_url.DS.$dir[$i]);
                    }
                }
            }
            if(count($this->cleandir)>0){
                $this->dir_loader();
            }
        }

        function dir_loader(){

                for($i=0;$i<count($this->cleandir);$i++){
                    $this->cur_url = $this->cleandir[$i];
                    unset($this->cleandir[$i]);
                    $this->cleandir = array_values($this->cleandir);
                    $this->dir_scan();
                }
        }

        function rewrite_cfg(){
            $file = fopen($this->cfg, 'w');
             fwrite($file, "[classes]\n");

            for($i=0; $i<count($this->files); $i++){
                fwrite($file, $i.' = '.$this->url.$this->files[$i]);
                fwrite($file, "\n");
            }

            fclose($file);
        }

        

    }