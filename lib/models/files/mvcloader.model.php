<?php


class mvcload{
    
    public $classname;
    
    function load($u){
        include_once(ROOT.DS.'app'.DS.$u.DS.$u.'.controller.php');
        
        $model = ROOT.DS.'app'.DS.$u.DS.$u.'.model.php';
        if(file_exists($model)){
            include_once($model);    
        }
        
        
        $this->classname = $u.'C';
        
        $C = new $this->classname;
        
    }
    
    function action($a){
        
        call_user_func(array($this->classname, $a));
    }
}