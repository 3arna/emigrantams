<?php
        //include_once(ROOT.DS.'lib'.DS.'models'.DS.'files'.DS.'filescaner.model.php');
        autoloadm(true);
        //autoloadc(false);

        function autoloadm($scan){
            
            $cfg = ROOT.DS.'cfg'.DS.'ml.ini.php';
            if($cfg){
                
                $ini = parse_ini_file($cfg);
                for($i=0; $i<count($ini); $i++){
                    include_once($ini[$i]);
                }
                
                if($scan){
                
                    $dir_path = ROOT.DS.'lib'.DS.'models';
                    $filter = new file_filter($dir_path, '.model', 'ml.ini.php');
                }
            }
        }
        function autoloadc($scan){
            
            $cfg = ROOT.DS.'cfg'.DS.'cl.ini.php';
            if($cfg){
                
                $ini = parse_ini_file($cfg);
                for($i=0; $i<count($ini); $i++){
                    include_once($ini[$i]);
                }
                
                if($scan){
                
                    $dir_path = ROOT.DS.'app';
                    $filter = new file_filter($dir_path, '.model', 'cl.ini.php');
                }
            }
        }

?>